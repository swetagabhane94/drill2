function alphabeticalSort(inventory) {
    let collect=[];
    for(let index in inventory) {
        collect.push(inventory[index].car_model);
    }
    collect.sort();
    return collect;
}

module.exports = alphabeticalSort;